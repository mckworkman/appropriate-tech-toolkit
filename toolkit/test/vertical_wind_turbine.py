"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
    This file holds the representative vertical wind turbine system that goes into the 
    model/db for testing purposes
    
    
    NEED TO CLEAN UP TRASH!!!

"""


from toolkit.models import *
import datetime
from termcolor import colored
import logging
from django.core.files import File
import os

class test:

    TEST_DIR = os.path.dirname(__file__)
    passed   = True
    ASSEMBLY = None
    
    def create_wind_turbine_assembly(self):
        """ 
        Add wind turbine assembly
        """
        TITLE = "Wind Turbine"
        DESC =  "A wind power system, is a power system designed \
        to supply usable solar power by means of mechanical motion. \
        It consists of an arrangement of several components, \
        including a blade used to convert wind energy \
        into mechanical rotaion, a generator to convert the \
        mechanical rotation into a AC current, as well as a tower, \
        guy wires and other accessories to set up \
        a working assembly. It may also use a tail vane \
        component to improve the assembly's overall performance and \
        include an integrated battery solution, as prices for \
        storage devices are expected to decline."
        try:
            self.ASSEMBLY = Assembly.objects.create_assembly(title=TITLE, description=DESC)
            # self.ASSEMBLY.display_assembly()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def create_blade_component(self):
        """
        Add blade component to the database
        """
        
        COMP = None

        TYPE           = COMP_TYPES[2][0]
        DESC           = "A blade is a streamlined body that generates lift \
        and rotate about its center"
        ICON_STR       = "turbine_blade"
        IMAGE          = os.path.join(self.TEST_DIR, '/images/test.jpg')
        DRAWING        = os.path.join(self.TEST_DIR, '/images/test.jpg')

        M_DIR_TITLE    = ["Optional Maintenance"]
        M_DIR_SUBTITLE = None
        M_DIR_DESC     = ["Blades require pretty low maintenance \
        occasional dusting of the blade may be required to keep the assembly \
        working at optimal performance."]
        M_DIR_IMAGE    = None
        M_DIR_DRAWING  = None

        A_DIR_TITLE    = ["Blade assembly:"]
        A_DIR_SUBTITLE = None
        A_DIR_DESC     = ["turbine blade printed or with the correct airfoil dimensions or prefabricated in factories"]
        A_DIR_IMAGE    = None
        A_DIR_DRAWING  = None
        
        D_DIR_TITLE = ["Recycling turbine blades"]
        D_DIR_SUBTITLE = None
        D_DIR_DESC     = ["Blade can be composted if made out of wood or send to your local recycling facility "]
        D_DIR_IMAGE    = None
        D_DIR_DRAWING  = None
        try:
            COMP = Component.objects.create_component(  c_type=TYPE, 
                                                        description=DESC,
                                                        icon_title=ICON_STR,
                                                        image=IMAGE, 
                                                        drawing=DRAWING)
            
            COMP.set_io_quantity(title='wind velocity', 
                                io='i', 
                                quant_list=[(0,9),(0,9),(0,9)], 
                                unit='m/s', 
                                coord_sys= 'r')
            COMP.set_io_quantity(title='blade angular velocity', 
                                io='o', 
                                quant_list=[(0, 1200),(0,0),(0,0)], 
                                unit='rad/s', 
                                coord_sys= 'r')
            COMP.set_io_quantity(title='torque', 
                                io='o', 
                                quant_list=[(0, 1500),(0,0),(0,0)], 
                                unit='Nm', 
                                coord_sys= 'r')
            
            COMP.set_direction(  categ=Direction.ASSEMBLE,
                                title_list=A_DIR_TITLE,
                                subtitle_list = A_DIR_SUBTITLE,
                                description_list=A_DIR_DESC,
                                image_list = A_DIR_IMAGE,
                                drawing_list = A_DIR_DRAWING )
            
            COMP.set_direction(  categ=Direction.MAINTAIN,
                                title_list=M_DIR_TITLE,
                                subtitle_list = M_DIR_SUBTITLE,
                                description_list=M_DIR_DESC,
                                image_list = M_DIR_IMAGE,
                                drawing_list = M_DIR_DRAWING)
                            
            COMP.set_direction(  categ=Direction.DISSASEMBLE,
                                title_list=D_DIR_TITLE,
                                subtitle_list = D_DIR_SUBTITLE,
                                description_list=D_DIR_DESC,
                                image_list = D_DIR_IMAGE,
                                drawing_list = D_DIR_DRAWING )
            
            COMP.set_purpose_method(purp='generate_torque', meth='aerodynamic_lift') # do we cover all the possible combination of words for this?
            
            self.ASSEMBLY.add_component_to_assembly(COMP)
            # self.ASSEMBLY.display_assembly()
            return COMP
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            if COMP != None:
                COMP.delete()
            return False

    def create_generator_system(self):
        """
        Add Sample Systems To Database
        """
        
        SYS = None

        # Create the system
        TYPE           = SYS_TYPES[0][0]
        DESC           = "A generator is an electrical motor that converts \
        the mechanical rotation of the blade into electricity"
                
        ICON_STR       = "turbine_generator"
        IMAGE          = os.path.join(self.TEST_DIR, '/images/test.jpg')
        DRAWING        = os.path.join(self.TEST_DIR, '/images/test.jpg')

        M_DIR_TITLE    = ["Optional Maintenance"]
        M_DIR_SUBTITLE = None
        M_DIR_DESC     = ["Generators require pretty low maintenance because they \
        are concidered closed systems. However, occasional greasing of the main shaft \
        a garden hose may be required."]
        M_DIR_IMAGE    = None
        M_DIR_DRAWING  = None

        A_DIR_TITLE    = ["Preassembled generator:"]
        A_DIR_SUBTITLE = None
        A_DIR_DESC     = ["turbine generators are preassembled in factories"]
        A_DIR_IMAGE    = None
        A_DIR_DRAWING  = None
        
        D_DIR_TITLE = ["Recycling turbine generators"]
        D_DIR_SUBTITLE = None
        D_DIR_DESC     = ["Generators should generally not be disassembled because \
        it can void your warrant and you are likely to risk damaging it. \
        The cell can be recycled however at locations that specialize in \
        Generator recycling."]
        D_DIR_IMAGE    = None
        D_DIR_DRAWING  = None
        try:
            SYS = System.objects.create_system( s_type=TYPE, 
                                                description=DESC,
                                                icon_title=ICON_STR,
                                                image=IMAGE, 
                                                drawing=DRAWING)
            
            SYS.set_io_quantity(title='torque', 
                                io='i', 
                                quant_list=[(0, 1500),(0,0),(0,0)], 
                                unit='Nm', 
                                coord_sys= 'r')
            SYS.set_io_quantity(title='heat', 
                                io='o', 
                                quant_list=[(0, 1200)], 
                                unit='J')
            SYS.set_io_quantity(title='energy', 
                                io='o', 
                                quant_list=[(330, 2280)], 
                                unit='kWh')
            
            SYS.set_direction(  categ=Direction.ASSEMBLE,
                                title_list=A_DIR_TITLE,
                                subtitle_list = A_DIR_SUBTITLE,
                                description_list=A_DIR_DESC,
                                image_list = A_DIR_IMAGE,
                                drawing_list = A_DIR_DRAWING )
            
            SYS.set_direction(  categ=Direction.MAINTAIN,
                                title_list=M_DIR_TITLE,
                                subtitle_list = M_DIR_SUBTITLE,
                                description_list=M_DIR_DESC,
                                image_list = M_DIR_IMAGE,
                                drawing_list = M_DIR_DRAWING)
                            
            SYS.set_direction(  categ=Direction.DISSASEMBLE,
                                title_list=D_DIR_TITLE,
                                subtitle_list = D_DIR_SUBTITLE,
                                description_list=D_DIR_DESC,
                                image_list = D_DIR_IMAGE,
                                drawing_list = D_DIR_DRAWING )
            
            SYS.set_purpose_method(purp='generate_power', meth='mechanical_rotation')
            
            self.ASSEMBLY.add_system_to_assembly(SYS)
            #SYS.display_system(with_io=True)
            return SYS
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            if SYS != None:
                SYS.delete()
            return False
    

# OUTSIDE OF CLASS TEST    
def run_all_tests():
    """
    Create a vertical wind turbine instance representative of all vertical 
    wind turbines.
    """
    
    t = test()
    
    # WIND TURBINE ASSEMBLY
    if t.create_wind_turbine_assembly():
        print colored("PASSED........................wind_turbine_assembly", "green")
    else:
        t.passed = False
        print colored("FAILED........................wind_turbine_assembly", "red")
    
    # BLADE
    if t.create_blade_component():
        print colored("PASSED........................blade_component", "green")
    else:
        t.passed = False
        print colored("FAILED........................blade_component", "red")
    
    # GENERATOR
    if t.create_generator_system():
        print colored("PASSED........................generator_system", "green")
    else:
        t.passed = False
        print colored("FAILED........................generator_system", "red")
    
    if t.passed:
        t.ASSEMBLY.display_assembly(show_complete=True)
    
    if t.passed == False:
        
        # Remove the assembly
        t.ASSEMBLY.component_set.all().delete()
        t.ASSEMBLY.system_set.all().delete()
        t.ASSEMBLY.delete()
        
    
    return t.passed    
    
"""
    For McK
    -------------------
    - need to add dimensions; shape such as airfoil, curve?
    - Purpose and method does not seem to save the nouns verbs or adj but saves all the None values
    - test passed when coord_type for vector quantity = None. error is raised but does not return false
    
    - Did not raise error when the quantity was not entered:
        ERROR:root:Exception: global name 'comp' is not defined
        Traceback (most recent call last):
        File "/home/ubuntu/workspace/toolkit/models.py", line 1024, in set_io_quantity
        self.quantity_set.add(comp)
    
    - individual create functions return false, but run_all_tests return true
    
    For SB
    -------------------
    add materials: create_material() takes at least 12 arguments (9 given);
    use purpose manager;  use system manager; add directions for system maintenance, assembly
    
"""

#     TITLE_NOSE_CONE = 'nose cone'
#     DESC_NOSE_CONE  = "Cone shaped body at the front end of the wind turbine \
#                       that has the purpose of reducing the total force on the \
#                       turbine and the aerodynamic drag of the blades."
    
#     TITLE_BLADE = 'Blade'
#     DESC_BLADE  = 'Streamlined body'
#     # DIM_BLADE  = 'Streamlined body'
#     # WT_BLADE  = 'Streamlined body'
#     # BLA_BLADE  = 'Streamlined body'
    
#     TITLE_NACELLE = 'Nacelle'
#     DESC_NACELLE  = 'Aparatus used to house the generator and other turbine systems'
    
#     TITLE_TOWER = 'Tower'
#     DESC_TOWER = 'Aparatus used to support the wind turbine'
    
#     TITLE_GUY_WIRES = 'Guy wires'
#     DESC_GUY_WIRES  = 'wires usedto hold down the main turbine tower'
    
#     TITLE_GUY_ANCHOR = 'Guy anchor'
#     DESC_GUY_ANCHOR  = 'Anchor used to hold the guy wires attached to the tower to the ground.'
    
#     TITLE_TAIL_VANE = 'Tail vane'
#     DESC_TAIL_VANE = 'Aparatus used to orient the turbine'
    
#     TITLE_GENERATOR = 'Generator'
#     DESC_GENERATOR = 'Aparatus used to generate electricity'
    
#     TITLE_SHAFT = 'Shaft'
#     DESC_SHAFT = 'metal rod'
    
#     TITLE_ASSEMB_DIR = 'Wind turbine assembly directions'
#     SUBTITLE_ASSEMB_DIR = 'Easy'
#     DESC_ASSEMB_DIR = 'README'

#     CAT = Direction.ASSEMBLE    
#     nose_cone_lifetime = datetime.timedelta(weeks=100)
#     blade_lifetime = datetime.timedelta(weeks=10)
#     nacelle_lifetime = datetime.timedelta(weeks=20)
#     tower_lifetime = datetime.timedelta(weeks=30)
#     guy_wires_lifetime = datetime.timedelta(weeks=208)
#     guy_anchor_lifetime = datetime.timedelta(weeks=104)
#     tail_vane_lifetime = datetime.timedelta(weeks=440)
#     generator_lifetime = datetime.timedelta(weeks=50)
#     shaft_lifetime = datetime.timedelta(weeks=60)
