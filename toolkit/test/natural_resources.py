"""

    Author: Benoit Sebastien, Mckinnley Workman
    Description: Add sample data to DB for testing purposes

"""
from toolkit.models import *
import datetime
from termcolor import colored
import logging
from django.core.files import File
import os

"""
Add Natural Resources To Database
"""

class test:
    
    passed = True

    def add_rainfall(self):
        """
        Add rainfall NR
        """
        try:
            r = NaturalResource.objects.create_natural_resource(    title="Rainfall", 
                                                                    description = 'Rainfall comes from the clouds and is a form of  ....wiki',
                                                                    icon_title = "rain")
            r.set_io_quantity(  quant_list=[(0,100)],
                                unit='m',
                                title='rain volume',
                                io='o')
            r.set_purpose_method('provide_water', 'gravitational_force')
            r.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
            
    def add_atmospheric_moisture(self):
        """
        Add Atmospheric Moisture
        """
        try: 
            m = NaturalResource.objects.create_natural_resource(    title="Atmospheric Moisture",
                                                                    description = 'Atmospheric Moisture comes from the clouds and is a form of  ....wiki',
                                                                    icon_title = "moisture")
            m.set_io_quantity(  title='humidity'
                                unit='%', #use % sign?
                                io='i',
                                quant_list=[(0,100)] )
            m.set_purpose_method('provide_moisture', 'gravitational_force')
            m.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_sun(self):
        """
        Add sun
        """
        try:
            s = NaturalResource.objects.create_natural_resource(    title='Solar Radiation',
                                                                    description = 'Solar Radiation comes from the sun and is a form of electromagnetic radiation ....wiki',
                                                                    icon_title = "sun" )
            s.set_io_quantity(  title='solar_radiation',
                                unit = 'W/m2',
                                io = 'o',
                                quant_list=[(0,1300)] )
            s.set_io_quantity(  title='heat',
                                unit='J',
                                io='o',
                                quant_list=[(0,1000000)]) #change this max number? WE SHOUD USE SOMETHING THAT THE USER CAN MEASURE
            s.set_purpose_method('provide_heat', 'electromagnetic_radiation')
            s.set_purpose_method('provide_light', 'electromagnetic_radiation')
            s.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_geothermal(self):
        """
        Add geothermal
        """
        try:
            g = NaturalResource.objects.create_natural_resource(    title='Geothermal',
                                                                    description = 'Geothermal Heat comes from thermal energy stored in the earth and is a form of heat transfer ....wiki'
                                                                    icon_title = "geothermal" )
            g.set_io_quantity(title='heat',
                              unit = 'J',
                              io   = 'o',
                              quant_list=[(190.35,355.15)])
            #g.set_io_quantity(title='soil_type', 'i', )
            g.set_io_quantity(title='solar radiation',
                              unit ='W/m2',
                              io = 'i',
                              quant_list=[(0,1300)])
            g.set_purpose_method('provide_heat', 'heat_conduction')
            g.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_wind(self):
        """
        Add wind
        """
        try:
            w = NaturalResource.objects.create_natural_resource(    title='Wind',
                                                                    description = 'Wind comes from temperature gradients and is a form of natural heat convection  ....wiki',
                                                                    icon_title = "wind" )
            w.set_io_quantity(  title='wind velocity',
                                unit='m/s',
                                coord_sys='r',
                                io='i',
                                quant_list=[(0,100),(0,100),(0,100)] ) 
            w.set_io_quantity(  title='force',
                                unit='N',
                                coord_sys='r',
                                io='o',
                                quant_list=[(0,100),(0,100),(0,100)]) #don't know max
            w.set_io_quantity(  title='wind velocity',
                                unit='m/s',
                                coord_sys='r',
                                io='o',
                                quant_list=[(0,100),(0,100),(0,100)] )
            w.set_purpose_method('move_air', 'temperature_gradient')
            w.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_lightning(self):
        """
        Lightning
        """
        try:
            l = NaturalResource.objects.create_natural_resource(    title='Lightning',
                                                                    description = 'Lightning comes from electric potential difference in the clouds and is a form of plasma ....wiki',
                                                                    icon_title = "lightning"
                                                                )
            l.set_io_quantity(  title='lighting storm frequency',
                                unit='%',
                                io='i',
                                quant_list=[(0,100)])
            l.set_io_quantity(  title='heat',
                                unit='J',
                                io='o',
                                quant_list=[(0.1,30000 )])
            l.set_io_quantity(  title='power',
                                unit='W',
                                io='o',
                                quant_list=[(0.1,1000000000000)])
            l.set_io_quantity(  title='light',
                                unit='lumen',
                                io='o',
                                quant_list=[(0,1000000000)]) # nearly imp to measure due tothe fact that no single strike happen at once and that the intensity decreasewith 1/r^2
            l.set_purpose_method('provide_heat', 'heat_conduction') 
            l.set_purpose_method('provide_power', 'potential_difference')
            l.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_river(self):
        """
        River
        """
        try:
            r = NaturalResource.objects.create_natural_resource(    title='River',
                                                                    description = 'Rivers come from ....wiki',
                                                                    icon_title = "river" )                        
            r.set_io_quantity(  title='flow_rate',
                                io='o',
                                quant_list=[(0.1,100)],
                                unit='m3/s')
            r.set_purpose_method('move_water', 'gravitational_force')
            r.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_waterfall(self): #encounter issue with the unit. how to define thedirection of a scalar
        """
        Waterfall
        """
        try:
            w = NaturalResource.objects.create_natural_resource(    title='Waterfall',
                                                                    icon_title = "waterfall"
                                                                    description = 'Waterfall come from ....wiki"
                                                                )
            w.set_io_quantity(  title='height',
                                io='i',
                                quant_list=[(0,0),(0,0),(0,1000)]
                                coord_sys='r'
                                unit='m')
            w.set_io_quantity(  title='gravity',
                                io='i',
                                quant_list=[(0),(0),(9.8)]
                                coord_sys='r'
                                unit='m/s2')
            w.set_io_quantity(  title='hydraulic head',
                                io='o',
                                quant_list=''
                                coord_sys=''
                                unit='')
            w.set_purpose_method('move_water', 'gravitational_force')
            w.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False

    def add_waves(self):
        """
        Waves
        """
        try:
            w = NaturalResource.objects.create_natural_resource(    title='Waves',
                                                                    icon_title = "waves",
                                                                    description = 'Waves comes from temperature gradients ....wiki"
                                                                )
            
            w.set_io_quantity(  title='wave length',
                                io='o',
                                quant_list=[(0,100)]
                                unit='m')
            w.set_io_quantity(  title='amplitude',
                                io='o',
                                quant_list=[(0,50)]
                                unit='m')
            w.set_io_quantity(  title='velocity',
                                io='o',
                                quant_list=[(0,200)]
                                unit='m/s')
            w.set_purpose_method('move_water', 'gravitational_force')
            w.save()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
            
def main():
    """
    Run all test functions
    """
    
    t = test()
    
    if t.add_rainfall():
        print colored("PASSED........................rainfall", "green")
    else:
        t.passed = False
        print colored("FAILED........................rainfall", "red")

    if t.add_atmospheric_moisture():
        print colored("PASSED........................atmospheric_moisture", "green")
    else:
        t.passed = False
        print colored("FAILED........................atmospheric_moisture", "red")
    
    if t.add_sun():
        print colored("PASSED........................sun", "green")
    else:
        t.passed = False
        print colored("FAILED........................sun", "red")
    
    if t.add_geothermal():
        print colored("PASSED........................geothermal", "green")
    else:
        t.passed = False
        print colored("FAILED........................geothermal", "red")
    
    if t.add_wind():
        print colored("PASSED........................wind", "green")
    else:
        t.passed = False
        print colored("FAILED........................wind", "red")
    
    if t.add_lightning():
        print colored("PASSED........................lightning", "green")
    else:
        t.passed = False
        print colored("FAILED........................lightning", "red")
        
    if t.add_river():
        print colored("PASSED........................river", "green")
    else:
        t.passed = False
        print colored("FAILED........................river", "red")
    
    if t.add_waterfall():
        print colored("PASSED........................waterfall", "green")
    else:
        t.passed = False
        print colored("FAILED........................waterfall", "red")
    
    if t.add_waves():
        print colored("PASSED........................waves", "green")
    else:
        t.passed = False
        print colored("FAILED........................waves", "red")
    
    return t.passed