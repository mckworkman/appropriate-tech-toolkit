"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
    This file holds the representative solar pv system that goes into the 
    model/db for testing purposes
    
    Solar PV Resources
    http://www.energy.ca.gov/reports/2001-09-04_500-01-020.PDF

    NEED TO CLEAN UP TRASH!!!

"""
from toolkit.models import *
import datetime
from termcolor import colored
import logging
from django.core.files import File
import os

class test:

    TEST_DIR = os.path.dirname(__file__)
    passed = True
    ASSEMBLY = None
    
    def create_solar_pv_assembly(self):
        # Create the Solar PV Assembly
        TITLE = "Solar PV"
        DESC =  "A photovoltaic system, also solar PV power \
                system, or PV system, is a power system designed \
                to supply usable solar power by means of photovoltaics. \
                It consists of an arrangement of several components, \
                including solar panels to absorb and convert sunlight \
                into electricity, a solar inverter to change the \
                electric current from DC to AC, as well as mounting, \
                cabling and other electrical accessories to set up \
                a working system. It may also use a solar tracking \
                system to improve the system's overall performance and \
                include an integrated battery solution, as prices for \
                storage devices are expected to decline. Strictly \
                speaking, a solar array only encompasses the ensemble \
                of solar panels, the visible part of the PV system, \
                and does not include all the other hardware, often \
                summarized as balance of system (BOS). "
        try:
            self.ASSEMBLY = Assembly.objects.create_assembly(title=TITLE, description=DESC)
            # self.ASSEMBLY.display_assembly()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
    
    def create_pv_module_system(self):
        """
        Create a single pv module system
        """
        
        SYS = None

        # Create the system
        TYPE = SYS_TYPES[4][0]
        DESC = "A PV module is a packaged, connected assembly of solar cells. \
        Solar panels are generally used as a component of a larger photovoltaic \
        system to generate and supply electricity in commercial and \
        residential applications. Polycrystalline silicon, also called \
        polysilicon or poly-Si, is a high purity, polycrystalline form \
        of silicon, used as a raw material by the solar photovoltaic \
        and electronics industry. Polysilicon is produced from \
        metallurgical grade silicon by a chemical purification process, \
        called Siemens process."
                
        ICON_STR = "pv_module"
        IMAGE = os.path.join(self.TEST_DIR, '/images/test.jpg')
        DRAWING = os.path.join(self.TEST_DIR, '/images/test.jpg')

        M_DIR_TITLE = ["Optional Maintenance"]
        M_DIR_SUBTITLE = None
        M_DIR_DESC  =   ["Solar Panels require pretty low maintenance because there \
        are not many moving parts. However, occasional rinsing with \
        a garden hose may be worth your while especially if you live \
        in an area with lots of dust."]
        M_DIR_IMAGE = None
        M_DIR_DRAWING = None

        A_DIR_TITLE = ["Preassembled Panels:"]
        A_DIR_SUBTITLE = None
        A_DIR_DESC  = ["Solar Panels are preassembled in factories"]
        A_DIR_IMAGE = None
        A_DIR_DRAWING = None
        
        D_DIR_TITLE = ["Recycling Solar Panels"]
        D_DIR_SUBTITLE = None
        D_DIR_DESC  = ["Solar Panels should generally not be disassembled because \
        it can void your warrant and you are likely to risk damaging it. \
        The cell can be recycled however at locations that speialize in \
        solar panel recycling."]
        D_DIR_IMAGE = None
        D_DIR_DRAWING = None
        try:
            SYS = System.objects.create_system( s_type=TYPE, 
                                                description=DESC,
                                                icon_title=ICON_STR,
                                                image=IMAGE, 
                                                drawing=DRAWING)
            
            SYS.set_io_quantity(title='solar radiation', 
                                io='i', 
                                quant_list=[(0, 1500)], 
                                unit='W/m2', 
                                coord_sys=None)
            SYS.set_io_quantity(title='heat', 
                                io='i', 
                                quant_list=[(0, 1200)], 
                                unit='J', 
                                coord_sys=None)
            SYS.set_io_quantity(title='power', 
                                io='o', 
                                quant_list=[(100, 3000)], 
                                unit='W', 
                                coord_sys=None)
            SYS.set_io_quantity(title='heat', 
                                io='o', 
                                quant_list=[(0, 100)], 
                                unit='J', 
                                coord_sys=None)
            
            SYS.set_direction(  categ=Direction.ASSEMBLE,
                                title_list=A_DIR_TITLE,
                                subtitle_list = A_DIR_SUBTITLE,
                                description_list=A_DIR_DESC,
                                image_list = A_DIR_IMAGE,
                                drawing_list = A_DIR_DRAWING )
            
            SYS.set_direction(  categ=Direction.MAINTAIN,
                                title_list=M_DIR_TITLE,
                                subtitle_list = M_DIR_SUBTITLE,
                                description_list=M_DIR_DESC,
                                image_list = M_DIR_IMAGE,
                                drawing_list = M_DIR_DRAWING)
                            
            SYS.set_direction(  categ=Direction.DISSASEMBLE,
                                title_list=D_DIR_TITLE,
                                subtitle_list = D_DIR_SUBTITLE,
                                description_list=D_DIR_DESC,
                                image_list = D_DIR_IMAGE,
                                drawing_list = D_DIR_DRAWING )
            
            SYS.set_purpose_method(purp='generate_voltage', meth='photoelectric_effect')
            
            self.ASSEMBLY.add_system_to_assembly(SYS)
            # self.ASSEMBLY.display_assembly()
            return SYS
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            if SYS != None:
                SYS.delete()
            return False
    
    def create_circuit_combiner_system(self):
        """
        """
        
        SYS = None
        
        TYPE = "circuit_combiner"
        ICON_STR = "circuit_combiner_icon"
        DESC =  "Look at a recent PV system schematic and youll see a component \
        between the PV array and the charge controller its called a combiner \
        box. It provides a place to join the array conductors to the \
        conductors that feed the electricity to the batteries."
        IMAGE = None
        DRAWING = None

        # Source: http://appropriatetec.appstate.edu/sites/appropriatetec.appstate.edu/files/BuildingaCombinerBox.pdf
        A_DIR_TITLE = ["Weather Proof Box", "Wiring", "Power Distribution Blocks"]
        A_DIR_SUBTITLE = None
        A_DIR_DESC  = ["If these combiner boxes are outside, they need to be \
        rainproof junction boxes. These should be UL-listed \
        boxes that have specially fitted or gasketed doors to \
        prevent the infiltration of moisture. The NEMA TYPE \
        3R, available from Grainger, satisfies these criteria and \
        is commonly used for this purpose. These boxes come \
        in a variety of sizes with side hinged doors, or in a less \
        expensive version with a screw-on cover. Both are \
        lockable. In an outdoor environment, wire access to the \
        box should only be through the bottom to minimize the \
        entry of water. A good size for the box would be 12 by \
        15 by 4 inches (30 x 38 x 10 cm). \
        Conduit and/or strain reliefs listed for outdoor use can \
        be easily attached to the box through the knockouts. \
        Conduit should be used with 90C rated conductors that \
        are not sunlight resistant such as the commonly \
        available single conductor stranded THWN-2. The \
        strain reliefs listed for outdoor use should be used when \
        conductors having sunlight resistance (such as USE-2 \
        or TC) are used and not run in conduit. Some of the \
        other components, such as a two-pole power \
        distribution block, DIN rail, and grounding terminal strip \
        should be securely attached to this box with stainless \
        steel fasteners.",
        "See image for wiring guidelines",
        "A power distribution block brings the conductors \
        together in a combiner box. Power distribution blocks \
        come in a large variety of sizes and configurations. \
        They can be single, double, or triple pole. They have a \
        large barrel-type connection on one side, and normally \
        four or six smaller barrel connections on the other side, \
        although quite a range of other options exist. \
        The primary or large terminal block for the outgoing \
        cables needs to be large enough for the size of cable \
        selected. You also need to be sure that there will be \
        enough spaces on the secondary side of the block for \
        the incoming array wires. Depending on how the array \
        is wired, this could mean four spaces for an eight \
        module 24 volt array one for each series string." ]
        A_DIR_IMAGE = [None, None, "images/combiner_box_wiring.PNG"]
        A_DIR_DRAWING = None

        M_DIR_TITLE = ["Optional Maintenance"]
        M_DIR_SUBTITLE = None
        M_DIR_DESC  =   ["Only maintenance required if box breaks or needs waterproofing"]
        M_DIR_IMAGE = None
        M_DIR_DRAWING = None


        try:
            SYS = System.objects.create_system( s_type=TYPE, 
                                                description=DESC,
                                                icon_title=ICON_STR,
                                                image=IMAGE, 
                                                drawing=DRAWING)
            
            SYS.set_direction(  categ=Direction.ASSEMBLE,
                                title_list=A_DIR_TITLE,
                                subtitle_list = A_DIR_SUBTITLE,
                                description_list=A_DIR_DESC,
                                image_list = A_DIR_IMAGE,
                                drawing_list = A_DIR_DRAWING )
            
            SYS.set_direction(  categ=Direction.MAINTAIN,
                                title_list=M_DIR_TITLE,
                                subtitle_list = M_DIR_SUBTITLE,
                                description_list=M_DIR_DESC,
                                image_list = M_DIR_IMAGE,
                                drawing_list = M_DIR_DRAWING)
                            
            SYS.set_purpose_method(purp='combine_conductors', meth='junction_box')
            
            self.ASSEMBLY.add_system_to_assembly(SYS)
            # self.ASSEMBLY.display_assembly()
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            if SYS != None:
                SYS.delete()
            return False
    
    def create_ground_fault_protector_system(self):
        """
        """
        SYS = None
        
        TYPE = "fuse"
        ICON_STR = "ground fault protector"
        DESC = "A GFCI is a ground fault circuit interrupter. A ground fault circuit \
        interrupter is an inexpensive electrical device that, if installed in \
        household branch circuits, could prevent over two-thirds of the approximately \
        300 electrocutions still occurring each year in and around the home. \
        Installation of the device could also prevent thousands of burn and \
        electric shock injuries each year."
        IMAGE   = None
        DRAWING = None
        
        
        try:
            SYS = System.objects.create_system( s_type=TYPE, 
                                                description=DESC,
                                                icon_title=ICON_STR,
                                                image=IMAGE, 
                                                drawing=DRAWING)
            
            SYS.set_purpose_method(purp='provide_safety', meth='conduction_interuption')
            
            self.ASSEMBLY.add_system_to_assembly(SYS)
            # self.ASSEMBLY.display_assembly(self.ASSEMBLY)
            return True
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            if SYS != None:
                SYS.delete()
            return False
        
    def create_dc_fused_switch_system(self):
        """
        """
        pass 
        
    def create_inverter_system(self):
        """
        """
        pass

    def create_ac_fused_switch_system(self):
        """
        """
        pass
        
    def create_utility_switch_system(self):
        """
        """
        pass
        
    def create_main_service_panel_system(self):
        """
        """
        pass
     
    def create_battery_system(self):
        """
        """
        pass
            
# OUTSIDE OF CLASS    
def run_all_tests():
    """
    Creates a complete example solar py system 
    
    http://www.energy.ca.gov/reports/2001-09-04_500-01-020.PDF
    
    """
    
    t = test()
    
    # SOLAR ASSEMBLY
    if t.create_solar_pv_assembly():
        print colored("PASSED........................solar_pv_assembly", "green")
    else:
        t.passed = False
        print colored("FAILED........................solar_pv_assembly", "red")
    
    # PV MODULE
    if t.create_pv_module_system():
        print colored("PASSED........................pv_module_system", "green")
    else:
        t.passed = False
        print colored("FAILED........................pv_module_system", "red")
    
    # CIRCUIT COMBINER
    if t.create_circuit_combiner_system():
        print colored("PASSED........................circuit_combiner_system", "green")
    else:
        t.passed = False
        print colored("FAILED........................circuit_combiner_system", "red")
    
    # GROUND FAULT PROTECTOR
    if t.create_ground_fault_protector_system():
        print colored("PASSED........................ground_fault_protector_system", "green")
    else:
        t.passed = False
        print colored("FAILED........................ground_fault_protector_system", "red")
    
    # # DC FUSED SWITCH
    # if t.create_dc_fused_switch_system():
    #     print colored("PASSED........................dc_fused_switch_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................dc_fused_switch_system", "red")
    
    # # DC/AC INVERTER
    # if t.create_inverter_system():
    #     print colored("PASSED........................inverter_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................inverter_system", "red")
    
    # # AC FUSED SWITCH
    # if t.create_ac_fused_switch_system():
    #     print colored("PASSED........................ac_fused_switch_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................ac_fused_switch_system", "red")
    
    # # UTILITY SWITCH
    # if t.create_utility_switch_system():
    #     print colored("PASSED........................utility_switch_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................utility_switch_system", "red")
    
    # # MAIN SERVICE PANEL
    # if t.create_main_service_panel_system():
    #     print colored("PASSED........................main_service_panel_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................main_service_panel_system", "red")
    
    # # BATTERY
    # if t.create_battery_system():
    #     print colored("PASSED........................battery_system", "green")
    # else:
    #     t.passed = False
    #     print colored("FAILED........................battery_system", "red")
    
    if t.passed:
        t.ASSEMBLY.display_assembly(show_complete=True)
    
    if t.passed == False:
        
        # Remove the assembly
        t.ASSEMBLY.system_set.all().delete()
        t.ASSEMBLY.delete()
        
    
    return t.passed