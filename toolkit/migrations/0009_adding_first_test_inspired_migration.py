# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0008_adding_natural_resource_its_manager_its_relationships_to_previous_class_V0_1'),
    ]

    operations = [
        migrations.AddField(
            model_name='direction',
            name='component',
            field=models.ForeignKey(blank=True, to='toolkit.Component', null=True),
        ),
        migrations.AlterField(
            model_name='method',
            name='method_adjective',
            field=models.CharField(max_length=30, choices=[(b'mechanical', b'mechanical'), (b'rotational', b'rotational'), (b'rotation', b'rotation'), (b'chemical', b'chemical'), (b'force', b'force'), (b'radiation', b'radiation'), (b'gradient', b'gradient'), (b'photoelectric', b'photoelectric'), (b'difference', b'difference'), (b'interuption', b'interuption'), (b'lift', b'lift'), (b'conduction', b'conduction')]),
        ),
        migrations.AlterField(
            model_name='method',
            name='method_noun',
            field=models.CharField(max_length=30, choices=[(b'vibration', b'vibration'), (b'rotation', b'rotation'), (b'water', b'water'), (b'air', b'air'), (b'heat', b'heat'), (b'temperature', b'temperature'), (b'power', b'power'), (b'electricity', b'electricity'), (b'effect', b'effect'), (b'moisture', b'moisture'), (b'electromagnetic', b'electromagnetic'), (b'gravitational', b'gravitational'), (b'safety', b'safety'), (b'conduction', b'conduction'), (b'torque', b'torque'), (b'mechanical', b'mechanical'), (b'aerodynamic', b'aerodynamic'), (b'conductor', b'conductor')]),
        ),
        migrations.AlterField(
            model_name='purpose',
            name='purp_noun',
            field=models.CharField(max_length=20, choices=[(b'vibration', b'vibration'), (b'rotation', b'rotation'), (b'water', b'water'), (b'air', b'air'), (b'heat', b'heat'), (b'temperature', b'temperature'), (b'power', b'power'), (b'electricity', b'electricity'), (b'effect', b'effect'), (b'moisture', b'moisture'), (b'electromagnetic', b'electromagnetic'), (b'gravitational', b'gravitational'), (b'safety', b'safety'), (b'conduction', b'conduction'), (b'torque', b'torque'), (b'mechanical', b'mechanical'), (b'aerodynamic', b'aerodynamic'), (b'conductor', b'conductor')]),
        ),
        migrations.AlterField(
            model_name='quantity',
            name='scalar_unit',
            field=models.CharField(blank=True, max_length=30, choices=[(b'power', (b'watt', b'W')), (b'irradiance', (b'irradiance', b'W/m2')), (b'work', (b'joule', b'J')), (b'length', (b'meter', b'm')), (b'mass', (b'kilogram', b'kg')), (b'volume', (b'liter', b'l')), (b'electric charge', (b'coulomb', b'C')), (b'electric potential', (b'voltage', b'v')), (b'resistance', (b'ohm', 'U+2126')), (b'electric conductance', (b'siemens', b'S')), (b'capacitance', (b'Farad', b'F')), (b'inductance', (b'Henry', b'H')), (b'frequency', (b'hertz', b'Hz')), (b'time', (b'second', b's')), (b'luminous intensity', (b'candela', b'cd')), (b'illuminance', (b'lux', b'lx')), (b'luminous flux', (b'lumen', b'lm')), (b'pressure', (b'pascal', b'Pa')), (b'magnetic flux', (b'weber', b'Wb')), (b'magnetic flux density', (b'tesla', b'T')), (b'activity', (b'bequerel', b'Bq')), (b'amount of substance', (b'mole', b'mol')), (b'absorbed dose', (b'gray', b'Gy')), (b'dose equivalent', (b'sievert', b'Sv')), (b'catalyctic activity', (b'katal', b'kat')), (b'density', (b'rho', b'kg/m3')), (b'persentage', (b'percent', b'%')), (b'temperature', (b'kelvin', b'K')), (b'energy', (b'kilowatt-hour', b'kWh')), (b'flow_rate', (b'meter cubed per second', b'm3/s'))]),
        ),
    ]
