# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0003_adding_component_its_manager_its_relationships_to_previous_class'),
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('common_name', models.CharField(max_length=30)),
                ('color', models.TextField(default=None, null=True, blank=True)),
                ('appearance', models.TextField(default=None, null=True, blank=True)),
                ('texture', models.TextField(default=None, null=True, blank=True)),
                ('odor', models.TextField(default=None, null=True, blank=True)),
                ('toxicity', models.IntegerField(blank=True, null=True, choices=[(b'1', b'I'), (b'2', b'II'), (b'3', b'III'), (b'4', b'IV')])),
                ('uses', models.TextField(default=None, null=True, blank=True)),
                ('image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('sustainability_rating', models.IntegerField(null=True, blank=True)),
                ('sustainability_notes', models.TextField(default=None, null=True, blank=True)),
                ('category', models.CharField(blank=True, max_length=20, choices=[(b'wood', b'wood'), (b'composite', b'composite'), (b'metal', b'metal'), (b'polymer', b'polymer'), (b'semiconductor', b'semiconductor'), (b'metamaterial', b'metamaterial'), (b'ceramic', b'ceramic'), (b'concrete', b'concrete'), (b'glass', b'glass'), (b'electronic/optical', b'electronic/optical')])),
                ('scientific_name', models.CharField(default=None, max_length=30, null=True, blank=True)),
                ('distribution', models.CharField(default=None, max_length=30, null=True, blank=True)),
                ('endgrain', models.TextField(default=None, null=True, blank=True)),
                ('rot_resistance', models.TextField(default=None, null=True, blank=True)),
                ('workability', models.TextField(default=None, null=True, blank=True)),
                ('crystal_structure', models.TextField(default=None, null=True, blank=True)),
                ('ferrous', models.NullBooleanField(default=None)),
            ],
        ),
        migrations.CreateModel(
            name='Quantity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vector', models.BooleanField(default=False)),
                ('scalar', models.BooleanField(default=False)),
                ('scalar_unit', models.CharField(blank=True, max_length=30, choices=[(b'power', (b'watt', b'W')), (b'irradiance', (b'irradiance', b'W/m2')), (b'work', (b'joule', b'J')), (b'length', (b'meter', b'm')), (b'mass', (b'kilogram', b'kg')), (b'volume', (b'liter', b'l')), (b'electric charge', (b'coulomb', b'C')), (b'electric potential', (b'voltage', b'v')), (b'resistance', (b'ohm', 'U+2126')), (b'electric conductance', (b'siemens', b'S')), (b'capacitance', (b'Farad', b'F')), (b'inductance', (b'Henry', b'H')), (b'frequency', (b'hertz', b'Hz')), (b'time', (b'second', b's')), (b'luminous intensity', (b'candela', b'cd')), (b'illuminance', (b'lux', b'lx')), (b'luminous flux', (b'lumen', b'lm')), (b'pressure', (b'pascal', b'Pa')), (b'magnetic flux', (b'weber', b'Wb')), (b'magnetic flux density', (b'tesla', b'T')), (b'activity', (b'bequerel', b'Bq')), (b'amount of substance', (b'mole', b'mol')), (b'absorbed dose', (b'gray', b'Gy')), (b'dose equivalent', (b'sievert', b'Sv')), (b'catalyctic activity', (b'katal', b'kat')), (b'density', (b'rho', b'kg/m3'))])),
                ('vector_unit', models.CharField(blank=True, max_length=30, choices=[(b'force', (b'newton', b'N')), (b'velocity', (b'mps', b'm/s')), (b'current', (b'amperage', b'amps')), (b'torque', (b'newton meter', b'Nm')), (b'angular velocity', (b'radian per second', b'rad/s'))])),
                ('coord_type', models.CharField(default=(b'r', b'rectangular coord system'), max_length=10, null=True, choices=[(b'r', b'rectangular coord system'), (b'p', b'polar coord system'), (b'c', b'cylindrical coord system')])),
                ('val_A_max', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('val_A_min', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('val_B_max', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('val_B_min', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('val_C_max', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('val_C_min', models.DecimalField(default=None, null=True, max_digits=15, decimal_places=5, blank=True)),
                ('io_type', models.CharField(default=b'', max_length=5, null=True, blank=True, choices=[(b'', b'None'), (b'i', b'input'), (b'o', b'output')])),
                ('required', models.NullBooleanField(default=False)),
                ('link_root', models.NullBooleanField(default=False)),
                ('component', models.ForeignKey(blank=True, to='toolkit.Component', null=True)),
                ('link_tip', models.ForeignKey(blank=True, to='toolkit.Quantity', null=True)),
                ('material', models.ManyToManyField(to='toolkit.Material', null=True, blank=True)),
                ('system', models.ForeignKey(blank=True, to='toolkit.System', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='material',
            name='CAS_number',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='average_dried_weight',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='component',
            field=models.ManyToManyField(to='toolkit.Component', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='material',
            name='crushing_strength',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='density',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='ductility',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='elastic_modulus',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='electric_conductivity',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='hardness',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='ionization_energy',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='janka_hardness',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='melting_point',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='modulus_of_elasticity',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='modulus_of_rupture',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='molecular_weight',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='refractive_index',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='shrinkage',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='specific_gravity',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='thermal_condutivity',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='tree_height',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='trunk_diameter',
            field=models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True),
        ),
    ]
