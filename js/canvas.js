/** 
*
* Author:
*
* Description: The canvas is the visual for creating and manipulating assemblies
*
*/


/**
 * Lay out background grid for different users
 * @param {String} user 
 * @return {Boolean} true
 */
function layout_background(user='maker') {
    
    var drawGridLines = function(num_rectangles_wide, num_rectangles_tall, boundingRect) {
    var width_per_rectangle = boundingRect.width / num_rectangles_wide;
    var height_per_rectangle = boundingRect.height / num_rectangles_tall;
    for (var i = 0; i <= num_rectangles_wide; i++) {
        var xPos = boundingRect.left + i * width_per_rectangle;
        var topPoint = new paper.Point(xPos, boundingRect.top);
        var bottomPoint = new paper.Point(xPos, boundingRect.bottom);
        var aLine = new paper.Path.Line(topPoint, bottomPoint);
        aLine.strokeColor = 'black';
    }
    for (var i = 0; i <= num_rectangles_tall; i++) {
        var yPos = boundingRect.top + i * height_per_rectangle;
        var leftPoint = new paper.Point(boundingRect.left, yPos);
        var rightPoint = new paper.Point(boundingRect.right, yPos);
        var aLine = new paper.Path.Line(leftPoint, rightPoint);
        aLine.strokeColor = 'black';
    }
    
    drawGridLines(4, 4, paper.view.bounds);
}
    // if (user == 'user') {
    //     add_item(location)
    //     add_item(outputs)
    // }
    
    return true;
}

/**
 * Puts an ITEM object on canvas when user clicks on toolbar
 * @param {} 
*/
function create_natural_resource(title, description, icon_title, purpose) {
    
    // on-hover show the description
    // on-click check validity
    // on-right-click and on double click show properties menu
    // make item draggable
    // on-hover-over-io, highlight show title
    // on-mouse-down-hover-over-io, highlight
}

// Create LINK object
    // on-io-click draw link
    // on-drag draw line
    // on-over-io-release, on-release

// Place ITEM on canvas when user clicks on an it in the toolbar
    // 
