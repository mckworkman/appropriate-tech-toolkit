"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
   
"""

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'at2.views.home', name='home'),
    url(r'^toolkit/', include('toolkit.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
