"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
   
"""

from django.shortcuts import render_to_response
from django.http import Http404

def home(request):
	return render_to_response('home.html')
